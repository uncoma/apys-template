Template para informes y presentaciones utilizado en la materia Aspectos Profesionales y Sociales.

Este trabajo fue iniciado a partir del Template básico aportado por Overleaf.

Se escribieron dos archivos principales:

- informe.tex
- presentacion.tex

Y se pueden compilar con los siguientes comandos:

```bash
pdflatex informe.tex
pdflatex presentacion.tex
```

Link GitLab: https://gitlab.com/uncoma/apys-template

Este obra está bajo [licencia Creative Commons Reconocimiento-CompartirIgual 4.0 Internacional](http://creativecommons.org/licenses/by-sa/4.0/).
